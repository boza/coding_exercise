require 'test_helper'

describe Job::Queue do
  
  it "should return empty string if no jobs are passed" do
    Job::Queue.new([]).sequence.must_equal ""
  end


  it "should return order list for jobs" do
    expected_string = "\nc => \nb => \na => c"
    Job::Queue.new([['a', 'c'], ['b'], ['c']]).sequence.must_equal expected_string
  end 

  it "should order jobs according to dependencies" do
    jobs = [ ['a'],
      ['b', 'c'],
      ['c', 'f'],
      ['d', 'a'],
      ['e', 'b'],
      ['f']
    ]
    Job::Queue.new(jobs).ordered_jobs.must_equal [["f"], ["a"], ["d", "a"], ["c", "f"], ["b", "c"], ["e", "b"]]
  end

  it "should raise expection if job depends on same job" do

    proc { Job::Queue.new([['a', 'a']]).sequence }.must_raise Job::SameJobDependency
  end 


  it "should raise expection if job circular dependency" do

    redundant_jobs = [['a'], ['b', 'c'], ['c', 'f'], ['d', 'a'], ['e'], ['f', 'b']]

    proc { Job::Queue.new(redundant_jobs).sequence }.must_raise Job::CircularDependency
  end 

end