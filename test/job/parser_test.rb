require 'test_helper'

describe Job::Parser do
  
  it "should return empty string if no jobs are passed" do
    Job::Parser.new('').parsed_jobs.must_equal []
  end

  it "should parse a list of jobs into array of arrays" do
    Job::Parser.new("
        a =>
        b =>
        c =>
    ").parsed_jobs.must_equal [['a'], ['b'], ['c']]
  end

  it "should parse a list of jobs into array of arrays" do
    Job::Parser.new("
        a => c
        b =>
        c =>
    ").parsed_jobs.must_equal [['a', 'c'], ['b'], ['c']]
  end

end