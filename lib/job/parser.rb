module Job
  class Parser

    attr_reader :parsed_jobs

    def initialize(jobs)
      @parsed_jobs = parse!(jobs)
    end

    def parse!(jobs)
      jobs.split(/\n/).map { |pair| pair.strip.split(/\s=>\s?/) }.reject(&:empty?)
    end

  end
end