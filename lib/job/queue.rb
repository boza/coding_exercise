module Job
  class Queue

    attr_reader :jobs
    
    def initialize(parsed_jobs)
      @jobs = parsed_jobs
    end
    
    def sequence
      raise SameJobDependency if jobs.any? { |job, dependency| job == dependency }
      jobs_stringify
    end

    def jobs_stringify
      "".tap do |s|
        ordered_jobs.each do |job, dependency|
          s << "\n#{job} => #{dependency}"
        end
      end
    end

    def ordered_jobs(order_jobs=[])
      count = 0
      loop do
        break if order_jobs.size == jobs.size
        raise CircularDependency if circular_dependency?(count += 1)     
        order_jobs = Queuer.new(jobs, order_jobs).queued_jobs
      end
      order_jobs
    end

    def circular_dependency?(count)
      count > jobs.size
    end
    
  end  
end