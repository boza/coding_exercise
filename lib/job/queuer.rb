module Job
  class Queuer

    attr_reader :jobs, :queued_jobs
    
    def initialize(jobs, queued_jobs)
      @jobs = jobs
      @queued_jobs = queued_jobs
      select_jobs
    end

    def select_jobs
      remaining_jobs.each do |job, dependency|
        select_job(job, dependency)
      end 
    end

    def select_job(job, dependency)
      if dependency.nil?
        queued_jobs.unshift [job]
      else
        insert_job_if_dependency_queued(job, dependency)
      end      
    end

    def insert_job_if_dependency_queued(job, dependency)
      queued_jobs << [job, dependency] if queued_jobs.map(&:first).flatten.include?(dependency)
    end

    def remaining_jobs
      jobs - queued_jobs
    end

  end
end