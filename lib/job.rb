Dir.glob('lib/*/*').each do |file|
  require "./#{file}"
end


module Job

  class SameJobDependency < StandardError;end
  class CircularDependency < StandardError;end

end


# Job::Queue.new(Job::Parser.new(jobs_string)).sequence